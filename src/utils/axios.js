import axios from "axios";

const API_TOKEN = process.env.REACT_APP_API_TOKEN;

const instance = axios.create({
  baseURL: "http://api.openweathermap.org/data/2.5/",
});

axios.defaults.headers.common = { Authorization: `Bearer ${API_TOKEN}` };

instance.interceptors.request.use((config) => {
  config = {
    ...config,
    url: `${config.url}&appid=${API_TOKEN}`,
  };

  return config;
});

instance.interceptors.response.use(
  function (response) {
    return response.data;
  },
  function (error) {
    if ([401, 403].indexOf(error.response.status) !== -1) {
      // auto logout if 401 Unauthorized or 403 Forbidden response returned from API
      alert(error.message);
      localStorage.clear();
      delete axios.defaults.headers.common["Authorization"];
      window.location.reload();
    } else {
      console.log("Error", error.message);
      return Promise.reject(error);
    }
  },
);

export default instance;
