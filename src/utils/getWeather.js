import axios from "./axios";

export const getWeather = async (queryParams) => {
  const response = await axios.get(`forecast?${queryParams}`);
  if (response) return response;
};
