import { render, screen } from "@testing-library/react";
import React from "react";
import App from "../components/App";

test("renders learn react link", () => {
  render(<App />);
  const element = screen.getByText(/React Weather App/i);
  expect(element).toBeInTheDocument();
});
