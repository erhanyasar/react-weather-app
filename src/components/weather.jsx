import moment from "moment";
import React from "react";
import Data from "../utils/city.list.json";
import { getWeather } from "../utils/getWeather";
import Search from "./search";

let temperatures = [],
  hours = [];

export default class Weather extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      responseObj: {},
      jsonData: [],
      userInput: "",
      isSuccessfulSearch: false,
      successfullySearchedCities: [],
    };
  }

  onInputChange = (event) =>
    this.setState({ [event.target.name]: event.target.value });

  // Feature: Not implemented but button could be disabled via adding 'disabled={this.props.isValid()}' attribute
  isValid = () => {
    let isSuccessfulParam = true;

    Data.forEach((element) => {
      // `city.list.json` under `src/utils` used to validate user input
      if (element.name === this.state.userInput) isSuccessfulParam = false;
    });

    return isSuccessfulParam;
  };

  submitSearch = async () => {
    if (!this.isValid())
      await getWeather(`q=${this.state.userInput}&units=metric`).then(
        (result) => {
          let tempArr = this.state.successfullySearchedCities;

          tempArr.push(this.state.userInput);
          this.setState(
            {
              successfullySearchedCities: tempArr,
              timeZone: result.city.timezone / 3600,
              jsonData: result.list,
              responseObj: result,
            },
            () => this.addRow(),
          );
        },
      );
    else alert("Lütfen doğru bir şehir adı girdiğinizden emin olun.");
  };

  addRow = () => {
    const temperatureData = this.state.jsonData.filter(
      (element, index) => index < 8,
    );
    const hourData = this.state.jsonData.filter((element, index) => index < 8);

    temperatureData.forEach((element) => temperatures.push(element.main.temp));
    hourData.forEach((element) => hours.push(element.dt_txt));

    this.handleReset();
  };

  // put unnecessarily to being able to render ui and clear after user input besides returning successful log message
  handleReset = () => this.setState({ isSuccessfulSearch: false });

  getDate = (hour) => {
    // moment.js makes hours ending with '01' instead of '00'
    let transformedHour = hour.slice(0, -3);
    let transformedHourStr = "";

    if (transformedHour > 12) {
      hour.slice(0, 2, toString(transformedHour));
      transformedHourStr = "0" + (transformedHour - 12) + ":00 PM";
    } else if (transformedHour === 12)
      transformedHourStr = transformedHour + ":00 PM";
    else if (transformedHour === 0) transformedHourStr = 12 + ":00 PM";
    else transformedHourStr = transformedHour + ":00 AM";

    return transformedHourStr;
  };

  render() {
    const renderWeatherDetails = this.state.successfullySearchedCities.map(
      (city, index) => {
        return (
          <tr key={index}>
            <th scope="row">
              {this.state.successfullySearchedCities[index]
                ? city + " Province"
                : ""}
            </th>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 0]
                  ? temperatures[index * 8 + 0] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 1]
                  ? temperatures[index * 8 + 1] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 2]
                  ? temperatures[index * 8 + 2] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 3]
                  ? temperatures[index * 8 + 3] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 4]
                  ? temperatures[index * 8 + 4] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 5]
                  ? temperatures[index * 8 + 5] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 6]
                  ? temperatures[index * 8 + 6] + " &#8451;"
                  : "",
              }}
            ></td>
            <td
              dangerouslySetInnerHTML={{
                __html: temperatures[index * 8 + 7]
                  ? temperatures[index * 8 + 7] + " &#8451;"
                  : "",
              }}
            ></td>
          </tr>
        );
      },
    );

    const renderTimeDetails = (
      <>
        <th scope="col">
          {hours[0] ? this.getDate(moment(hours[0]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[1] ? this.getDate(moment(hours[1]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[2] ? this.getDate(moment(hours[2]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[3] ? this.getDate(moment(hours[3]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[4] ? this.getDate(moment(hours[4]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[5] ? this.getDate(moment(hours[5]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[6] ? this.getDate(moment(hours[6]).format("HH:MM")) : ""}
        </th>
        <th scope="col">
          {hours[7] ? this.getDate(moment(hours[7]).format("HH:MM")) : ""}
        </th>
      </>
    );

    return (
      <>
        <nav
          className="navbar navbar-light bg-light"
          style={{ borderTop: "2px solid #000" }}
        >
          <span className="navbar-brand mb-0 h1">React Weather App</span>
        </nav>
        <Search
          onInputChange={this.onInputChange}
          isValid={this.isValid}
          submitSearch={this.submitSearch}
        />
        <div className="offset-2 col-8 my-5">
          <div className="row">
            <div className="col-12">
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">City</th>
                    {renderTimeDetails}
                  </tr>
                </thead>
                <tbody>{renderWeatherDetails}</tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}
