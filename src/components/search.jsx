import React from "react";

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.searchInput.focus();
  }

  render() {
    return (
      <>
        <div className="offset-2 col-8 my-5">
          <div className="row">
            <div className="col-8">
              <input
                type="text"
                className="form-control"
                name="userInput"
                placeholder="City"
                ref={(e) => (this.searchInput = e)}
                onChange={(e) => this.props.onInputChange(e)}
              />
            </div>
            <div className="col-4">
              <button
                type="button"
                className="btn btn-primary btn-block"
                style={{ float: "right" }}
                onClick={this.props.submitSearch}
              >
                Search
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

Search.propTypes = {
  onInputChange: () => {},
  submitSearch: () => {}
}