import React from "react";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Weather from "./weather";

function App() {
  return (
    <div className="App">
      <Weather />
    </div>
  );
}

export default App;
