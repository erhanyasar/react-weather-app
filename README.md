# Weather App

## Table of Contents

- Table of Contents
  - [Preface](#-preface)
  - [Getting Started](#-getting-started)
  - [Scripts Overview](#-scripts-overview)
  - [Deployment](#-deployment)
  - [Learn More](#learn-more)

## ✨ Preface

- Check out [**Weather App live**](https://weather-app-erhanyasar.vercel.app/) without hassling the setup process below.
- Visit and follow [**Erhan Yaşar**](https://gitlab.com/erhanyasar) to see more repositories like this.

### Given Brief

Using the Free open weather map api (http://openweathermap.org/forecast5 - you can obtain limited free api key only with registering with them)
 
  * Build an application that allows you to search the weather forecast for a city,
  * Build the application using React.js preferably using hooks (not mandatory),
  * Please consider local time,
  * Every time a new city is searched - Add to a table of cities displaying the next 24 hours weather forecast,
  * Complete the components (search, results and the weather container),
  * Temprature must be in Celsius.

## 🎯 Getting Started

To get started with this project, follow these steps:

1. Fork & clone repository:

- Open a terminal window within your preferred directory and then execute the command below to download the repository;

```bash
git clone https://gitlab.com/erhanyasar/weather-app.git
```

2. Install the dependencies:

- Then, execute one of the commands below within the same terminal window to install project dependencies;

```bash
npm i
# or
yarn
# or
pnpm i
# or
bun i
```

3. Run the development server via executing one of the commands below within your terminal:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

4. Configure environment variable:

- There is an `.env.example` in the root directory you can use for reference.
- Go to "http://api.openweathermap.org/" and sign up to create an 'API Key'.
- Then copy the API key you created and save it to `.env.example` file under the root directory.
- Lastly duplicate `.env.example` either manually or via running the command below as `.env.local`.

```bash
cp .env.example .env.local
```

- How should `.env.local` file should look like after placing the API key ('xxxxxxxxxxxxxx' below refers to your key);

```bash
REACT_APP_API_TOKEN=xxxxxxxxxxxxxx
```

5. Finally, open [http://localhost:3000](http://localhost:3000) with your browser to see live result.

## 📃 Scripts Overview

The following scripts are available in the `package.json`:

- `start`:
    - Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
    - The page will reload if you make edits. You will also see any lint errors in the console.
- `build`:
    - Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.
    - The build is minified and the filenames include the hashes. Your app is ready to be deployed!
    - See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
- `test`:
    - Launches the test runner in the interactive watch mode.
    - See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
- `eject`: **Note: this is a one-way operation. Once you `eject`, you can’t go back!**
    - If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.
    - Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.
    - You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## 📤 Deployment

Easily deploy your React.js app with Vercel by clicking [![Vercel](https://vercel.com/button)](https://vercel.com/)

## 🚀 Learn More

### Getting Started with Create React App
- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
- To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)