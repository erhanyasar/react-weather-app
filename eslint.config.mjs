import pluginJs from "@eslint/js";
import pluginReact from "eslint-plugin-react";
import globals from "globals";

export default [
  {files: ["**/*.{js,mjs,cjs,jsx}"]},
  {settings: {
    react: {
      version: "detect",
    },
  }},
  {languageOptions: { globals: { ...globals.node, ...globals.browser }}},
  pluginJs.configs.recommended,
  pluginReact.configs.flat.recommended,
];